#!/usr/bin/env python3

import gdb.types

def print_struct(name):
    struct = gdb.lookup_type(name)
    print(name)
    print("size %d" % struct.sizeof)
    print("\t\t\t\t\t\toffset\tsize")

    for k, v in gdb.types.deep_items(struct):
        field = struct[k]

        print("\t%-40s%d\t%d" % (field.name,
                                 field.bitpos / 8,
                                 field.type.sizeof))

    print()

print_struct("struct v4l2_ctrl_h264_decode_params")
print_struct("struct v4l2_h264_dpb_entry")
print_struct("struct v4l2_ctrl_h264_slice_params")
print_struct("struct v4l2_h264_pred_weight_table")
print_struct("struct v4l2_h264_weight_factors")
print_struct("struct v4l2_ctrl_h264_scaling_matrix")
print_struct("struct v4l2_ctrl_h264_pps")
print_struct("struct v4l2_ctrl_h264_sps")

gdb.execute("quit")
