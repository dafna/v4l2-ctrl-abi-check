#! /bin/bash

declare -A compiler

LANG=C
CFLAGS="-g"
OUTPUT=builds

compiler[arm]="arm-linux-gnueabihf-gcc"
compiler[arm64]="aarch64-linux-gnu-gcc"
compiler[x86]="gcc -m32"
compiler[x64]="gcc -m64"

function error_exit {
    echo "$1" >&2   ## Send message to stderr. Exclude >&2 if you don't want it that way.
    exit
}

function run_arch {
    COMPILER=${compiler[$1]}
    SCRIPT=$2
    INPUT=$3
    FILE=$OUTPUT/$arch-$(basename -s .c $INPUT)
    STRUCTURE=$FILE-structures

    command -v $COMPILER &> /dev/null || error_exit "$COMPILER not found"
    test -f $INPUT || error_exit "$INPUT not found"

    $COMPILER $CFLAGS $INPUT -o $FILE
    gdb -q --command $SCRIPT $FILE | tail -n +2 > $STRUCTURE
    HASH=$(sha1sum $STRUCTURE | cut -d ' ' -f 1)
    echo -e "$arch:\t$STRUCTURE\n\tSHA1: $HASH"
}

mkdir -p $OUTPUT

# H264
echo "H-264................."
for arch in "${!compiler[@]}"
do
    run_arch $arch h264-gdb.py h264.c
done

# MPEG-2
echo "MPEG-2................"
for arch in "${!compiler[@]}"
do
    run_arch $arch mpeg2-gdb.py mpeg2.c
done

# VP8
echo "VP8..................."
for arch in "${!compiler[@]}"
do
    run_arch $arch vp8-gdb.py vp8.c
done
